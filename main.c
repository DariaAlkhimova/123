#include <stdio.h>
#include "calculate.h"

int
main (void)
{
  float Numeral;
  char Operation[4];
  float Result;
  printf("Chislo: ");
  scanf("%f", &Numeral);
  printf("Operation (+,-,*,/,pow,sqrt,cos,sin,tan): ");
  scanf("%s", &Operation);
  Result = Calculate(Numeral, Operation);
  printf("%6.2f\n", Result);
  printf("Hello, world");
  return 0;
}
